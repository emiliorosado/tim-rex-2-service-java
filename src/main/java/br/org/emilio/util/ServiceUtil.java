package br.org.emilio.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.org.emilio.cpy.InputCpy;
import br.org.emilio.cpy.ReturnCpy;
import br.org.emilio.cpy.SystemInfoCpy;

@Component
public class ServiceUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceUtil.class);

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${core.url}")
	private String url;
	
	@Value("${core.pass.phrase}")
	private String passPhrase;
	
	
	public ReturnCpy call(String transaction, List<InputCpy> inputs, Map<Integer, Class<?>> mapRet) {
		SystemInfoCpy info = new SystemInfoCpy();
		info.setTransaction(transaction);
		info.setUser("DUMMY");
		info.setNumIp("0.0.0");
		info.setPassPhrase(passPhrase);
		
		String input = "0" + ObjToCblUtil.convert(info, SystemInfoCpy.class) + "\n";
		for ( InputCpy inp : inputs ) {
			Object obj = inp.getData();
			input += inp.getIdRecord() + ObjToCblUtil.convert(obj, obj.getClass()) + "\n";
		}
		
		String[] output;
		try {
			output = restTemplate.exchange(
					url, HttpMethod.POST, new HttpEntity<String>(input), String.class)
				.getBody().replaceAll("\r", "").split("\n");
		} catch (Exception e) {
			LOGGER.error("Erro ao acionar o servico core business");
			LOGGER.error("Transacao: " + info.getTransaction());
			LOGGER.error("SysInfo: " + info);
			LOGGER.error("", e);
			throw new RuntimeException(e);
		}
		
		ReturnCpy rtn = null;
		List<Object> objectsRtn = new ArrayList<Object>();
		
		for ( String out : output ) {
			int idMapa;
			try {
				if (out.length() > 0) {
					idMapa = Integer.parseInt(out.substring(0, 1), 10);
				} else {
					idMapa = -1;
				}
			} catch (NumberFormatException e ) {
				idMapa = -1;
			}
			
			String dadosCpy;
			if (out.length() > 1) {
				dadosCpy = out.substring(1);
			} else {
				dadosCpy = "";
			}
			
			if (idMapa == 0) {
				rtn = CblToObjUtil.convert(dadosCpy, ReturnCpy.class);
			} else if (idMapa > 0) {
				Object obj = CblToObjUtil.convert(dadosCpy, mapRet.get(idMapa));
				objectsRtn.add(obj);
			}
		}
		
		if (rtn == null) {
			LOGGER.error("O servico core business não retornou codigo retorno");
			LOGGER.error("Transacao: " + info.getTransaction());
			LOGGER.error("SysInfo: " + info);
			throw new RuntimeException();
		}
		
		if (rtn.getReturnCode() == 0) {
			if (objectsRtn.size() == 0) {
				rtn.setData(null);
			} else if (objectsRtn.size() == 1) {
				rtn.setData(objectsRtn.get(0));
			} else {
				rtn.setData(objectsRtn);
			}
		} else {
			rtn.setData(null);
		}

		return rtn;
	}
	
	
	public ReturnCpy call(String transaction, List<InputCpy> inputs, Class<?> clazz) {
		Map<Integer, Class<?>> map = new HashMap<Integer, Class<?>>();
		map.put(1, clazz);
		
		return this.call(transaction, inputs, map);
	}
	
	
	public ReturnCpy call(String transaction, InputCpy input, Map<Integer, Class<?>> mapRet) {
		List<InputCpy> list = new ArrayList<InputCpy>();
		list.add(input);
		
		return this.call(transaction, list, mapRet);
	}
	
	
	public ReturnCpy call(String transaction, InputCpy input, Class<?> clazz) {
		Map<Integer, Class<?>> map = new HashMap<Integer, Class<?>>();
		map.put(1, clazz);

		List<InputCpy> list = new ArrayList<InputCpy>();
		list.add(input);

		return this.call(transaction, list, map);
	}
	
	
	public ReturnCpy call(String transaction, Object object, Map<Integer, Class<?>> mapRet) {
		InputCpy input = new InputCpy();
		input.setIdRecord(1);
		input.setData(object);
		
		List<InputCpy> list = new ArrayList<InputCpy>();
		list.add(input);

		return this.call(transaction, list, mapRet);
	}	
	
	
	public ReturnCpy call(String transaction, Object object, Class<?> clazz) {
		Map<Integer, Class<?>> map = new HashMap<Integer, Class<?>>();
		map.put(1, clazz);

		InputCpy input = new InputCpy();
		input.setIdRecord(1);
		input.setData(object);

		List<InputCpy> list = new ArrayList<InputCpy>();
		list.add(input);

		return this.call(transaction, list, map);
	}	
	
}
