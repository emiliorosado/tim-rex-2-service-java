package br.org.emilio.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.Charset;

import br.org.emilio.annotation.CblField;

public class ObjToCblUtil {

	public static String convert(Object object, Class<?> clazz) {
		String cblDados = "";
		
		for (Field field : clazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(CblField.class)) {
				CblField annot = field.getAnnotation(CblField.class);
				Method getter = getGetter(clazz, field);
				Class<?> type = field.getType();
				int length = annot.length();
				int decimals = annot.decimals();
				
				if (annot.type().equals(CblField.Type.ALPHA)) {
					String valor = toString(object, getter, type);
					cblDados += tratarAlpha(valor, length);
				} else if (annot.type().equals(CblField.Type.NUMERIC)) {
					String valor = toString(object, getter, type);
					cblDados += tratarNumeric(valor, length, decimals);
				}
			}
		}
		
		return cblDados;
	}
	
	
	private static String toString(Object object, Method getter, Class<?> type) {
		Object valorField;
		try {
			valorField = getter.invoke(object);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		String valor;
		
		if (valorField == null) {
			valor = "";
		} else if (type == int.class || type == Integer.class) {
			valor = ((Integer) valorField) + "";
		} else if (type == long.class || type == Long.class) {
			valor = ((Long) valorField) + "";
		} else if (type == float.class || type == Float.class) {
			valor = ((Float) valorField) + "";
		} else if (type == double.class || type == Double.class) {
			valor = ((Double) valorField) + "";
		} else if (type == String.class) {
			valor = (String) valorField;
		} else {
			valor = "";
		}
		
		return valor;
	}
	
	
	private static Method getGetter(Class<?> clazz, Field field) {
		String nomeCampo = field.getName();
		String nomeMetodo = "get" 
				+ nomeCampo.substring(0, 1).toUpperCase()
				+ nomeCampo.substring(1);
		Method metodo;
		try {
			 metodo = clazz.getMethod(nomeMetodo);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return metodo;
	}
	
	
	private static String charRepeat(char chr, int tamanho) {
		String str = "";
		
		for ( int i = 0 ; i < tamanho ; i++ ) {
			str += chr;
		}
		
		return str;
	}
	
	
	private static String tratarAlpha(String valor, int tamanho) {
		int tamanhoValor = valor.length();
		
		if (tamanhoValor > tamanho) {
			valor = valor.substring(0, tamanho);
		} else if (tamanhoValor < tamanho) {
			valor += charRepeat(' ', tamanho - tamanhoValor);
		}
		
		try {
			Charset cs = Charset.forName("ISO-8859-1");
			valor = new String(valor.getBytes(cs), cs);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
		
		return valor;
	}
	
	
	private static String tratarNumeric(String valor, int tamanhoInteiro, int tamanhoDecimal) {
		int posPonto = valor.indexOf('.');
		String valorInteiro;
		String valorDecimal;
		
		if (posPonto != (-1)) {
			valorInteiro = valor.substring(0, posPonto);
			valorDecimal = valor.substring(posPonto + 1);
		} else {
			valorInteiro = valor;
			valorDecimal = "";
		}
		
		int tamanhoValorInteiro = valorInteiro.length();
		int tamanhoValorDecimal = valorDecimal.length();
		
		if (tamanhoValorInteiro > tamanhoInteiro) {
			valorInteiro = valorInteiro.substring(tamanhoValorInteiro - tamanhoInteiro);
		} else if (tamanhoValorInteiro < tamanhoInteiro) {
			valorInteiro = charRepeat('0', tamanhoInteiro - tamanhoValorInteiro) + valorInteiro;
		}
		
		if (tamanhoValorDecimal > tamanhoDecimal) {
			valorDecimal = valorDecimal.substring(0, tamanhoDecimal);
		} else if (tamanhoValorDecimal < tamanhoDecimal) {
			valorDecimal = valorDecimal + charRepeat('0', tamanhoDecimal - tamanhoValorDecimal);
		}
		
		return valorInteiro + valorDecimal;
	}

}
