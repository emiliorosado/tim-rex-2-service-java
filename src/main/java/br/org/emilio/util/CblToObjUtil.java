package br.org.emilio.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import br.org.emilio.annotation.CblField;

public class CblToObjUtil {

	@SuppressWarnings("deprecation")
	public static <T> T convert(String linha, Class<T> clazz) {
		T objDados;
		try {
			objDados = clazz.newInstance();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		int posicao = 0;
		
		for (Field field : clazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(CblField.class)) {
				CblField annot = field.getAnnotation(CblField.class);
				Class<?> type = field.getType();
				Method setter = getSetter(clazz, field, type);
				int length = annot.length();
				int decimals = annot.decimals();
				
				if (annot.type().equals(CblField.Type.ALPHA)) {
					objDados = tratarAlpha(objDados, linha, setter, posicao, length);
					posicao += length;
				} else if (annot.type().equals(CblField.Type.NUMERIC)) {
					objDados = tratarNumeric(objDados, linha, setter, type, posicao, length, decimals);
					posicao += length + decimals;
				}
			}
		}
		
		return objDados;
	}
	
	
	private static Method getSetter(Class<?> clazz, Field field, Class<?> type) {
		String nomeCampo = field.getName();
		String nomeMetodo = "set" 
				+ nomeCampo.substring(0, 1).toUpperCase()
				+ nomeCampo.substring(1);
		Method metodo;
		try {
			 metodo = clazz.getMethod(nomeMetodo, type);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return metodo;
	}
	
	
	private static String charRepeat(char chr, int tamanho) {
		String str = "";
		
		for ( int i = 0 ; i < tamanho ; i++ ) {
			str += chr;
		}
		
		return str;
	}
	
	
	private static <T> T tratarAlpha(T objDados, String linha, Method setter, int posicao, int tamanho) {
		if (objDados == null) {
			return null;
		}
		
		if (linha == null) {
			linha = "";
		}
		
		int posicaoInicial = posicao;
		if (posicaoInicial > linha.length()) {
			posicaoInicial = linha.length();
		}
		
		int posicaoFinal = posicaoInicial + tamanho;
		if (posicaoFinal > linha.length()) {
			posicaoFinal = linha.length();
		}
		
		String valor = linha.substring(posicaoInicial, posicaoFinal);
		
		try {
			setter.invoke(objDados, valor.trim());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return objDados;
	}
	
	
	private static <T> T tratarNumeric(T objDados, String linha, Method setter, Class<?> type, int posicao, int tamanhoInteiro, int tamanhoDecimal) {
		if (objDados == null) {
			return null;
		}
		
		if (linha == null) {
			linha = "";
		}
		
		int tamanho = tamanhoInteiro + tamanhoDecimal;
		
		int posicaoInicial = posicao;
		if (posicaoInicial > linha.length()) {
			posicaoInicial = linha.length();
		}
		
		int posicaoFinal = posicaoInicial + tamanho;
		if (posicaoFinal > linha.length()) {
			posicaoFinal = linha.length();
		}
		
		double valor;
		try {
			valor = Double.parseDouble(linha.substring(posicaoInicial, posicaoFinal));
		} catch (NumberFormatException e) {
			throw new RuntimeException(e);
		}
		
		int dividir = Integer.parseInt("1" + charRepeat('0', tamanhoDecimal), 10);
		valor = valor / dividir;
		
		try {
			if (type == int.class || type == Integer.class) {
				setter.invoke(objDados, (int) valor);
			} else if (type == long.class || type == Long.class) {
				setter.invoke(objDados, (long) valor);
			} else if (type == float.class || type == Float.class) {
				setter.invoke(objDados, (float) valor);
			} else {
				setter.invoke(objDados, valor);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return objDados;
	}
	
}
