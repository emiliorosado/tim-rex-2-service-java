package br.org.emilio.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Emilio Rosado | 11 99967 5980 | emlrosado1@gmail.com
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CblField {
	public enum Type {ALPHA, NUMERIC};
	Type type();
	int length();
	int decimals() default 0;
}
