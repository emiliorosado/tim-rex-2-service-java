package br.org.emilio.cpy;

import br.org.emilio.annotation.CblField;
import br.org.emilio.annotation.CblField.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SystemInfoCpy {

	@CblField(type = Type.ALPHA, length = 1)
	private String option;
	
	@CblField(type = Type.NUMERIC, length = 10)
	private int empresa;
	
	@CblField(type = Type.ALPHA, length = 10)
	private String user;
	
	@CblField(type = Type.ALPHA, length = 10)
	private String transaction;
	
	@CblField(type = Type.ALPHA, length = 20)
	private String numIp;
	
	@CblField(type = Type.ALPHA, length = 100)
	private String passPhrase;
	
	@CblField(type = Type.ALPHA, length = 100)
	private String appHomePath;
}
