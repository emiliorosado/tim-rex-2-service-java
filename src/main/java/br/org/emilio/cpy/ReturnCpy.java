package br.org.emilio.cpy;

import br.org.emilio.annotation.CblField;
import br.org.emilio.annotation.CblField.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReturnCpy {

	@CblField(type = Type.NUMERIC, length = 3)
	private int returnCode;
	
	@CblField(type = Type.ALPHA, length = 100)
	private String message;
	
	private Object data;
}
