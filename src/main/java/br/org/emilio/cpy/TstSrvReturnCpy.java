package br.org.emilio.cpy;

import br.org.emilio.annotation.CblField;
import br.org.emilio.annotation.CblField.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TstSrvReturnCpy {

	@CblField(type = Type.NUMERIC, length = 10)
	private int soma;

	@CblField(type = Type.NUMERIC, length = 10)
	private int subtracao;

	@CblField(type = Type.NUMERIC, length = 10)
	private int multiplicacao;

	@CblField(type = Type.NUMERIC, length = 10, decimals = 5)
	private double divisao;
	
	@CblField(type = Type.ALPHA, length = 100)
	private String observacao;
}
