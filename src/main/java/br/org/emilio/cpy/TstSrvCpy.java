package br.org.emilio.cpy;

import br.org.emilio.annotation.CblField;
import br.org.emilio.annotation.CblField.Type;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TstSrvCpy {

	@CblField(type = Type.NUMERIC, length = 10)
	private int numero1;

	@CblField(type = Type.NUMERIC, length = 10)
	private int numero2;
	
	@CblField(type = Type.ALPHA, length = 100)
	private String observacao;
}
