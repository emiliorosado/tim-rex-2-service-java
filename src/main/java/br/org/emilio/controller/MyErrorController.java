package br.org.emilio.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.org.emilio.cpy.ReturnCpy;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;

@RestController
public class MyErrorController implements ErrorController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(MyErrorController.class);

	
	@RequestMapping("/error")
	public ReturnCpy error(HttpServletRequest request) {
		int statusCode = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		String message;
		
		if (statusCode == 401 || statusCode == 403) {
			message = "ACESSO NAO PERMITIDO";
		} else if (statusCode == 404) {
			message = "RECURSO NAO ENCONTRADO";
		} else {
			Exception e = (Exception) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
			LOGGER.error("Erro na chamada do servico", e);
			message = "SISTEMA INDISPONIVEL";
		}
		
		ReturnCpy ret = new ReturnCpy();
		ret.setReturnCode(999);
		ret.setMessage(message);
		ret.setData(null);
		
		return ret;
	}
	
}
