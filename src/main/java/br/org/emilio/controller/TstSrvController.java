package br.org.emilio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.org.emilio.cpy.ReturnCpy;
import br.org.emilio.cpy.TstSrvCpy;
import br.org.emilio.cpy.TstSrvReturnCpy;
import br.org.emilio.util.ServiceUtil;

@RestController
@RequestMapping("/tst_srv")
public class TstSrvController {

	@Autowired
	private ServiceUtil service;

	
	@PostMapping
	public ReturnCpy teste(@RequestBody TstSrvCpy cpy) {
		return this.service.call("tst-srv", cpy, TstSrvReturnCpy.class);
	}

}
